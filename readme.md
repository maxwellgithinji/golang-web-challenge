# Web development challenge
In the following challenge, you will be required to test and implement a processor that aggregates data efficiently.

## Introduction

The current implementation generates millions of plays for 10000 songs. You are required to calculate the top-rated song. You are also required to test for this.

The test requires you to implement `processors/interface.go` and `processors/processor.go`.

You are also required to test the functioning of the processor by implementing `main_test.go`


### Requirements

1. Running main.go prints out the top rated song. Calculate the average rating by referring to Rating fields of the Play struct.
2. Running main_test.go (`go test`) validates that the processor calculates the above correctly.
3. Memory usage should be below 128mb. Extra points awarded for less usage.
4. Modifying other parts of the code is allowed but ensure the basics of the challenge remain the same.


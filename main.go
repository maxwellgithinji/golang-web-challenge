package main

import (
	"fmt"
	"golangchallenge/processors"
	"math/rand"
	"sync"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/google/uuid"
)

func GetPlaysData(wg *sync.WaitGroup) *processors.PlaysData {
	data := &processors.PlaysData{}
	// Generate 10000 songs
	data.Songs = make([]*processors.Song, 0)
	for i := 0; i < 10000; i++ {
		data.Songs = append(data.Songs, &processors.Song{
			Id:   uuid.NewString(),
			Name: gofakeit.Name(),
		})
	}

	// Create a channel for plays with 1000 buffer
	data.Plays = make(chan *processors.Play, 1000)
	wg.Add(1)
	go func() {
		// Generate 10000000 plays
		for i := 0; i < 10000000; i++ {
			song := data.Songs[rand.Intn(len(data.Songs))]
			data.Plays <- &processors.Play{
				Id:         uuid.NewString(),
				SongId:     song.Id,
				SongRating: float64(rand.Intn(6)),
				Status:     "complete",
				Song:       song,
			}
		}
		wg.Done()
	}()
	return data
}

func main() {
	wg := &sync.WaitGroup{}
	data := GetPlaysData(wg)
	processor := processors.CreateProcessorFromData(data, wg)
	err := processor.StartProcessing()
	if err != nil {
		fmt.Printf("Error while processing data: %s\n", err)
		return
	}
	// Wait for processor to finish processing data
	wg.Wait()
	topSong := processor.GetTopRankedSong()
	fmt.Printf("Top song found: %s\n", topSong)
}

package processors

type Song struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type Play struct {
	Id         string  `json:"id"`
	SongId     string  `json:"song_id"`
	SongRating float64 `json:"song_rating"`
	Status     string  `json:"status"`
	Song       *Song   `json:"-"`
}

type PlaysData struct {
	Songs []*Song    `json:"songs"`
	Plays chan *Play `json:"-"`
}

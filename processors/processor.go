package processors

import (
	"sync"
)

// Processor should implement ProcessorInterface
// Hint: Try using goroutines to process the data in parallel
// Requirement: Do not store all plays in memory and cache your results
type Processor struct {
	data *PlaysData
	wg   *sync.WaitGroup
}

func (p Processor) StartProcessing() error {

	return nil
}
func (p Processor) GetTopRankedSong() *SongRanking {

	return nil
}

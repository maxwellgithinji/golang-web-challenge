package processors

import (
	"fmt"
	"sync"
)

type SongRanking struct {
	// Fill in your properties here
	song *Song
	rank float64
}

func (d *SongRanking) String() string {
	// Implement this function
	return fmt.Sprintf("%v Rating - %2f", d.song.Name, d.rank)
}

type ProcessorInterface interface {
	StartProcessing() error
	GetTopRankedSong() *SongRanking
}

func CreateProcessorFromData(data *PlaysData, wg *sync.WaitGroup) ProcessorInterface {
	// @todo Initialize your processor here
	return Processor{
		data: data,
		wg:   wg,
	}
}

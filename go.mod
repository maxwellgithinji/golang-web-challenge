module golangchallenge

go 1.18

require (
	github.com/brianvoe/gofakeit/v6 v6.19.0
	github.com/google/uuid v1.3.0
)
